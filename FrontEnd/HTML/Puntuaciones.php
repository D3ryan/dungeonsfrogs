<!DOCTYPE html>
<html lang="en">
<head>
     <!-- Required meta tags -->
     <title>Puntuaciones - Dungeons&Frogs</title>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <!-- Bootstrap CSS -->
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
     <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
     <link rel="stylesheet" href="../CSS/Puntuaciones.css">
     <link rel="icon" href="../imgs/_icono_pag.png">
</head>
<body>
     <main class="main-container container-fluid vh-100">
          <div class="row col-12">
               <div class="col-12">
                    <h1 class="df_titulo text-center">Puntuaciones</h1>
               </div>
          </div>
          <div class="df_container container text-center">
               <div class="row">
                    <div class="heads-up col-4">
                         Posición
                    </div>
                    <div class="heads-up col-4">
                         Usuario
                    </div>
                    <div class="heads-up col-4">
                         Puntuación
                    </div>
               </div>

               <?php 
               include '../PHP/connection.php';
                $db = new Database();
                $connection = $db->ConnectDB();
                $sql = "SELECT * FROM users ORDER BY user_score DESC LIMIT 3";
                $result = mysqli_query($connection, $sql);

                if(mysqli_num_rows($result) > 0){
                    $puesto = 1;
                    while($row = mysqli_fetch_assoc($result)){

                ?>
               <div class="row tabla-puntuaciones">
                    <div class="col-4 text-center text-puntuaciones">
                         <?php echo $puesto?>°
                    </div>
                    <div class="col-4 text-center">
                         <img class="img-usuario-puntuaciones" src="../imgs/usuario1.png" alt="">
                         <label class="nom-usuario-puntuaciones d-block"><?php echo $row['username'] ?></label> 
                    </div>
                    <div class="col-4 text-puntuaciones">
                         <?php echo $row['user_score'] ?>
                    </div>
               </div>
          <?php     
                    $puesto++;
                    }
               } 
               mysqli_close($connection);
               ?>
               
          </div>
          <div class="df_button-container d-flex justify-content-center mt-4">
               <a href="../HTML/MenuInicial.html" class="df_link-container">
                    <img class="df_marco-btn" src="../imgs/Marco_botones.png" alt="">
                    <button type="button" class="df_btn btn">REGRESAR</button>
               </a>
          </div>
     </main>
     <audio id="Audio_btn" src="../sound/Boton_sonido.mp3"></audio>
     <audio id="Audio_btn2" src="../sound/Boton_sonido2.mp3"></audio>
     <audio id="Back_music" loop src="../sound/Background_music.mp3"></audio>
     <!-- Optional JavaScript -->
     <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
     <script type="module" src="../JS/Utilidad.js"></script>
</body>
</html>