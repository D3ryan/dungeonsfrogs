window.fbAsyncInit = function(){
    FB.init({
        appId   : '411521327108788',
        xfbml   : true,
        version : 'v12.0'
    });

    FB.AppEvents.logPageView();
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if(d.getElementById(id)){return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js,fjs);
}(document, 'script', 'facebook-jssdk'));

function shareScore(score) { 
    FB.ui({
        method: 'share',
        href: 'https://festive-thompson.167-172-238-60.plesk.page/HTML/MenuInicial.html', //Aqui iria la url de nuestro host
        hashtag: "#Dungeons&Frogs",
        quote: "¡Logré completar un nivel de Dungeons&Frogs!"
    }, function(response){});
}

$(document).ready(function () {
    $("#shareButton").click(function (e) { 
        shareScore(100); //Aqui le mandaremos el score
    });
});