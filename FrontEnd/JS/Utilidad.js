
//MENU INICIAL--------------
//--------------------------
//--------------------------
//--------------------------

//AUDIO BOTON HOVER Y CLICK MENU INICIAL
//--------------------------

import LocalStorage from "../JS/localstorage.js"

var audio_btn = $("#Audio_btn")[0];
var audio2_btn = $("#Audio_btn2")[0];

$(".df_link-container").hover(function(){
     audio_btn.play();
     audio_btn.volume = 0.5;
});

$(".df_link-container").click(function(){
     audio2_btn.play();
     audio2_btn.volume = 0.6;
});

//AUDIO MUSICA BACKGROUND MENU INCIAL
//--------------------------
var music = $("#Back_music")[0];

$(document).ready(function(){
     
     let configuration = LocalStorage.read();

     if(configuration === null || (configuration !== null && configuration.Musica === 'true')){
          music.play();
          music.volume = 0.2;
          $("#music_checkbox").attr("checked", true);
     }

     $("#music_checkbox").on("change", function () {
          if(this.checked){
               music.play();
               
               let savedConfiguration = LocalStorage.read();

               if(savedConfiguration !== null) {
                    savedConfiguration.Musica = true;
               } else {
                    savedConfiguration = {}
                    savedConfiguration.Musica = true;
               }
               
               LocalStorage.save(savedConfiguration);
          }else{

               music.pause();

               let savedConfiguration = LocalStorage.read();

               if(savedConfiguration !== null) {
                    savedConfiguration.Musica = false;
               } else {
                    savedConfiguration = {};
                    savedConfiguration.Musica = false;
               }

               LocalStorage.save(savedConfiguration);
          }
     });

      
     
  
});



//MENU PUNTUACIONES---------
//--------------------------
//--------------------------
//--------------------------




//PANTALLA EMPEZAR----------
//--------------------------
//--------------------------
//--------------------------
// var jugador1_btn = $("#btn_jugador1");
// var jugador2_btn = $("#btn_jugador2");

// jugador1_btn.hover(function(){
//      $(".jugador1img").append("<img src='../imgs/1jugador.png'>");
// });
// jugador2_btn.hover(function(){
//      $(".jugador2img").append("<img src='../imgs/2jugador.png'>");
// });