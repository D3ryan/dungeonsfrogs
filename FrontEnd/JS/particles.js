
import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.118/build/three.module.js'

class Particles{

    constructor(color, size, texture, particleCount, particleDuration){
        this.geometry = new THREE.Geometry();
        this.material = new THREE.PointsMaterial({
            color: color,
            size: size,
            map: THREE.ImageUtils.loadTexture(texture),
            blending: THREE.AdditiveBlending,
            transparent: true
        });
        this.particleCount = particleCount;
        this.particleDuration = particleDuration;
        this.particleSystem = null;
        this.isGeneratingParticles = false;
        this.animCounter = 0;
    }

    spawnParticles() {
        for (let i = 0; i < this.particleCount; i++) {
            
            let px = this.getRandomArbitrary(-3, 3);
            let py = this.getRandomArbitrary(-3, 3);
            let pz = this.getRandomArbitrary(-3, 3);
            
            let particle = new THREE.Vector3(px, py, pz);

            this.geometry.vertices.push(particle);
        }

        this.particleSystem = new THREE.Points(this.geometry, this.material);


        return this.particleSystem;
    }


    getRandomArbitrary(min, max){
        return Math.random() * (max - min) + min;
    }
}

export default Particles;