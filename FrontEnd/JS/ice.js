// Librerías
import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.118/build/three.module.js'
import {OrbitControls} from '../three.js-master/examples/jsm/controls/OrbitControls.js'
import {GLTFLoader} from '../three.js-master/examples/jsm/loaders/GLTFLoader.js'
import {OBJLoader} from '../three.js-master/examples/jsm/loaders/OBJLoader.js'
import {MTLLoader} from '../three.js-master/examples/jsm/loaders/MTLLoader.js'
import {FBXLoader} from 'https://cdn.jsdelivr.net/npm/three@0.118.1/examples/jsm/loaders/FBXLoader.js'

import Particles from "../JS/particles.js";
import LocalStorage from "../JS/localstorage.js"
import { Vector3 } from '../three.js-master/build/three.module.js'

// Variables generales
var scene;
var camera;
var renderer;
var controls;
var clock;
var deltaTime;
var gameFinished = false;
var keys = {};
var coop = false;
var worldReady = false;
var renderers=[];
var players=[];
var enemies=[];
var rayCaster;
var objetosConColision = [];
var itemsCollision = [];
var objetosColisionJugadores = [];
var oldPosition = [];

var isMoving = false;
var isMoving2 = false;

//Audio
var footstepsAudio;
var levelMusic
var swordslash;
var helpgrog;
var healing;

//Particulas
var particles;
var healingparticle;
var speedparticle;
var healthparticle;
var strengthparticle;

//Particulas para calar, no iran aqui
var fireparticle;
var iceparticle;


//Sistema de particulas
var particleHealingSystem;
var particleSpeedSystem;
var particleHealthSystem;
var particleStrengthSystem;
var particleFireSystem;
var particleIceSystem;
var particleSystem;

//Animaciones
var animaciones_grog = [];
var enemyForward = true;

let _APP = null;
let skybox;
let materialArray = [];
let cameras=[];

var speed = 5;
const TURN_SPEED = THREE.Math.degToRad(3);
const PLAYER_1 = 0;
const PLAYER_2 = 1;
var player1_damage = 5

//Player Variables
var player1Health = 100;
var player2Health = 70;
var isAttacking1 = false;
var isAttacking2 = false;
var attackFlag = false;
var healFlag = false; 
var playerInRange = false; 

// Enemy Variables
var enemyHealth = 20;
var enemyAnimations = [];
var deadEnemy = false;
var enemyScalar = 0.035

const ENEMY_SPEED = 3;
const ENEMY_DAMAGE = 15;

const ENEMY_IDLE = 0;
const ENEMY_DEAD = 1;
const ENEMY_WALKING = 2;
const ENEMY_ATTACK = 3;

// Constantes para animaciones
const PLAYER_1_IDLE = 0;
const PLAYER_1_WALK = 1;
const PLAYER_1_ATTACK = 4; 

const PLAYER_2_IDLE = 2;
const PLAYER_2_WALK = 3;
const PLAYER_2_HEAL = 5;
const PLAYER2_HEALING = 5;

//Luz Focal
//Luces para cada escenario
const GREEN_LIGHT = new THREE.Color(0,0.5,0);
const RED_LIGHT = new THREE.Color(0.5,0.0,0);
const LIGHTBLUE_LIGHT = new THREE.Color(0.0,0.8,0.9);

//Estas son variables constantes para la luz focal
const distance = 100.0;
const angle = THREE.Math.degToRad(90);
const penumbra = 0.5;
const decay = 1.0;

var score = 0;

// Inicio
$(document).ready(function() {
     setupAudio();
     setupScene();
     document.addEventListener('keydown', onKeyDown);
     document.addEventListener('keyup', onKeyUp);	
});

function setupAudio() {

     footstepsAudio = $("#Footsteps")[0];

     levelMusic = $("#Back_musicFrozen")[0];
     swordslash = $("#Swordslash")[0];
     helpgrog = $("#help")[0];
     healing = $("#healing")[0];

     let configuration = LocalStorage.read();

     if(configuration === null || (configuration !== null && configuration.Musica === 'true')){
          levelMusic.play();
          levelMusic.volume = 0.4;
          $("#music_checkbox").attr("checked", true);
     }
     
}


$("#music_checkbox").on("change", function () {
     if(this.checked){
          levelMusic.play();
          
          let savedConfiguration = LocalStorage.read();

          if(savedConfiguration !== null) {
               savedConfiguration.Musica = true;
          } else {
               savedConfiguration = {};
               savedConfiguration.Musica = true;
          }
          
          LocalStorage.save(savedConfiguration);
     }else{
          levelMusic.pause();

          let savedConfiguration = LocalStorage.read();

          if(savedConfiguration !== null) {
               savedConfiguration.Musica = false;
          } else {
               savedConfiguration = {};
               savedConfiguration.Musica = false;
          }

          LocalStorage.save(savedConfiguration);
     }
});

function setupScene(){

     $(".df_score").text(score);
     $("#input_score").val(score);

     let savedConfiguration = LocalStorage.read();

     if(savedConfiguration !== null){
          if(savedConfiguration.Coop !== undefined && savedConfiguration.Coop === "true"){
               coop = true;
          }
     }

     // clock setup
     clock = new THREE.Clock();		
     
     // scene setup
     scene = new THREE.Scene();
     
     // camera setup
     createCamera();
     if(coop) // Si jugaremos en co-op, necesitaremos otra cámara
     createCamera();
     
     // renderer
     createRender();
     if(coop)
     createRender();// Si jugaremos en co-op, necesitaremos otro render
     
     // Grid
     /*var grid = new THREE.GridHelper(50, 10, 0xffffff, 0xffffff);
     grid.position.y = -1;
     scene.add(grid);*/
     
     
     //resize window
     // window.addEventListener('resize', function(){
     // var screenW = window.innerWidth/2;
     // var screenH = window.innerHeight;
     // renderer.setSize(screenW, screenH);
     // cameras[0].aspect = screenW / screenH;
     // cameras[0].updateProjectionMatrix();
     // });
     
     //controls(orbit)
     //controls = new OrbitControls(camera, renderer.domElement);
     
     // SkyBox
     createSkyBox();
     let skyboxGeo = new THREE.BoxGeometry(10000, 10000, 10000);
     skybox = new THREE.Mesh(skyboxGeo, materialArray);
     scene.add(skybox);
     
     
     // Luz ambiental
     var ambientLight = new THREE.AmbientLight(new THREE.Color(0.3,0.3,0.3), 0.8)
     scene.add(ambientLight)
     
     //Instanciamos nuestras luces, TODO: aqui variara el color dependiendo del escenario 
     var spotlights = [
          new THREE.SpotLight(LIGHTBLUE_LIGHT, 1.0, distance, angle, penumbra, decay),
          new THREE.SpotLight(LIGHTBLUE_LIGHT, 1.0, distance, angle, penumbra, decay),
          new THREE.SpotLight(LIGHTBLUE_LIGHT, 1.0, distance, angle, penumbra, decay),
          new THREE.SpotLight(LIGHTBLUE_LIGHT, 1.0, distance, angle, penumbra, decay),
          new THREE.SpotLight(LIGHTBLUE_LIGHT, 1.0, distance, angle, penumbra, decay)
     ];

     //Seteamos las posiciones de cada luz
     spotlights[0].position.set(5, 10, -20);
     spotlights[0].target.position.set(5, 0, -20);

     spotlights[1].position.set(40, 10, -10);
     spotlights[1].target.position.set(40, 0, -10);

     spotlights[2].position.set(5, 10, -60);
     spotlights[2].target.position.set(5, 0, -60);

     spotlights[3].position.set(5, 10, -100);
     spotlights[3].target.position.set(5, 0, -100);

     spotlights[4].position.set(-20, 10, -80);
     spotlights[4].target.position.set(-20, 0, -80);

     //Cada luz en nuestro arreglo la a;adimos a la escena.
     spotlights.forEach(spotlight => {
          scene.add(spotlight);
          scene.add(spotlight.target);
     });


     healingparticle = new Particles(new THREE.Color(0.9, 0.7, 0.0), 1, "../imgs/healing.png", 30, 3);
     speedparticle = new Particles(new THREE.Color(0.2,0.5,1), 1, "../imgs/speedparticle.png", 30, 6);
     healthparticle = new Particles(new THREE.Color(0.8, 0.0, 0.2), 1, "../imgs/lifeparticle.png", 30, 6);
     strengthparticle = new Particles(new THREE.Color(1.0,0.0,0.0), 1, "../imgs/rageparticle.png", 20, 6);
     
     //Estas particulas no iran aqui solo las calare
    
     iceparticle = new Particles(new THREE.Color(.2, 1, 1), 1.5, "../imgs/iceparticle.png", 20, 3);
     ///

     //Health cube
     const geometry = new THREE.BoxGeometry( 3, 5, 3 );
     const material = new THREE.MeshBasicMaterial({
          color: new THREE.Color(1.0,1.0,1.0)
     });
     material.transparent = true;
     material.opacity = 0.0;
     const HEALTH_CUBE = new THREE.Mesh( geometry, material );

     //Speed cube
     const SPEED_CUBE = HEALTH_CUBE.clone();
     //STRENGTH cube
     const STRENGTH_CUBE = HEALTH_CUBE.clone();

     //WIN CUBE
     const geometry_win = new THREE.BoxGeometry( 5, 5, 5 );
     const material_win = new THREE.MeshBasicMaterial({
          color: new THREE.Color(1.0,1.0,1.0)
     });
     
     material_win.transparent = true;
     material_win.opacity = 0.0;

     const WIN_CUBE = new THREE.Mesh( geometry_win, material_win );
     WIN_CUBE.position.set(-20.0, 2.0, -100.0);
     WIN_CUBE.name = "win_zone";
     
     scene.add(WIN_CUBE);
               
     //DUNGEON
     loadOBJWithMTL("../objs/dungeon_ice/", "Dungeon_ice.obj", "Dungeon_ice.mtl", (dungeon) =>{
          dungeon.scale.set(5, 5, 5);
          dungeon.position.z = 5;
          dungeon.position.x = -10;
          dungeon.position.y = -.5;
     
          scene.add(dungeon);
          objetosConColision.push(dungeon);
          worldReady=true;
     });
     loadOBJWithMTL("../objs/dungeon_ice/candelabro/", "candelabro_ice.obj", "candelabro_ice.mtl", (candelabro_ice) =>{
          candelabro_ice.scale.set(5, 5, 5);
          candelabro_ice.position.z = 5;
          candelabro_ice.position.x = -10;
          candelabro_ice.position.y = -.5;
     
          scene.add(candelabro_ice);
          // objetosConColision.push(candelabro_fire);
          worldReady=true;
     });
     loadOBJWithMTL("../objs/dungeon_ice/ladrillo/", "ladrillos_ice.obj", "ladrillos_ice.mtl", (ladrillos_ice) =>{
          ladrillos_ice.scale.set(5, 5, 5);
          ladrillos_ice.position.z = 5;
          ladrillos_ice.position.x = -10;
          ladrillos_ice.position.y = -.5;
     
          scene.add(ladrillos_ice);
          // objetosConColision.push(dungeon);
          worldReady=true;
     });
     loadOBJWithMTL("../objs/dungeon_ice/tronco_cadena/", "tronco_cadena_ice.obj", "tronco_cadena_ice.mtl", (tronco_cadena_ice) =>{
          tronco_cadena_ice.scale.set(5, 5, 5);
          tronco_cadena_ice.position.z = 5;
          tronco_cadena_ice.position.x = -10;
          tronco_cadena_ice.position.y = -.5;
     
          scene.add(tronco_cadena_ice);
          // objetosConColision.push(dungeon);
          worldReady=true;
     });

     loadOBJWithMTL("../objs/items/item_danio/", "item_danio.obj", "item_danio.mtl", (item_danio) => {
          item_danio.scale.set(1, 1, 1);
          item_danio.position.z = 0;
          item_danio.position.x = 8;
          item_danio.position.y = 20;
          
          scene.add(item_danio);
          
          item_danio.add(STRENGTH_CUBE);

          STRENGTH_CUBE.name = "strength_item";
          STRENGTH_CUBE.position.set(-3,0,-3);
          STRENGTH_CUBE.grab = false;

          itemsCollision.push(STRENGTH_CUBE);
     });

     loadOBJWithMTL("../objs/items/item_velocidad/", "item_velocidad.obj", "item_velocidad.mtl", (item_velocidad) => {
          item_velocidad.scale.set(1, 1, 1);
          item_velocidad.position.z = -9;
          item_velocidad.position.x = 46;
          item_velocidad.position.y = -0.5;

          scene.add(item_velocidad);
          item_velocidad.add(SPEED_CUBE);
          
          SPEED_CUBE.name = "speed_item";
          SPEED_CUBE.position.set(-3,0, 2.1);
          SPEED_CUBE.grab = false;
          itemsCollision.push(SPEED_CUBE);

     });

     loadOBJWithMTL("../objs/items/item_vida/", "item_vida.obj", "item_vida.mtl", (item_vida) => {
          item_vida.scale.set(1, 1, 1);
          item_vida.position.z = -85;
          item_vida.position.x = 20;
          item_vida.position.y = -0.5;

          scene.add(item_vida);

          item_vida.add(HEALTH_CUBE);

          HEALTH_CUBE.name = "health_item";
          HEALTH_CUBE.grab = false;
          itemsCollision.push(HEALTH_CUBE);
     });
     
     rayCaster = new THREE.Raycaster();
     
     cameras[PLAYER_1].vector = [
          new THREE.Vector3(0,0,1),//front
          new THREE.Vector3(0,0,-1),//back
          new THREE.Vector3(1,0,0),//right
          new THREE.Vector3(-1,0,0)//left
     ];
     
     
     if(coop)
          cameras[PLAYER_2].vector = [
               new THREE.Vector3(0,0,1),//front
               new THREE.Vector3(0,0,-1),//back
               new THREE.Vector3(1,0,0),//right
               new THREE.Vector3(-1,0,0)//left
          ];
     
     _APP = new LoadModelDemo();
     render();
     
     $("#scene_section").append(renderers[0].domElement);
     if(coop)
     $("#scene_section_2").append(renderers[1].domElement);
}
                    
//scene.background = new THREE.Color(0xffb000);
// var light = new THREE.DirectionalLight(0xffffff, 2);
// light.position.set(0,0,2);
// scene.add(light);


// Key down y Key up
function onKeyDown(event) {
     keys[String.fromCharCode(event.keyCode)] = true;
}
     
function onKeyUp(event) {
     keys[String.fromCharCode(event.keyCode)] = false;
     isMoving = false;
     isMoving2 = false;
}
     
function render(){
     requestAnimationFrame(render);
     deltaTime = clock.getDelta();	
          
     for(var i=0; i<players.length; i++){
          players[i].yaw = 0;
          players[i].forward = 0;
     }

     if(player1Health <= 0){
          $('#df_score-container').fadeIn(500);
          clock.stop();
     }
     // var yaw = 0;
     // var forward = 0;

     // Necesito obtener la posición de ambos jugadores antes de que se muevan, esto en caso que colisionen. 
     if(worldReady){
          oldPosition[PLAYER_1] = scene.getObjectByName("anchor");

          if(coop)
          oldPosition[PLAYER_2] = scene.getObjectByName("anchor2");
     }

     //player1
     if (keys["A"]) {
          players[PLAYER_1].rotation.y += TURN_SPEED;

     } else if (keys["D"]) {
          players[PLAYER_1].rotation.y -= TURN_SPEED;
     }
     if (keys["W"]) {
          players[PLAYER_1].forward = -speed;   
          isMoving = true;
          animaciones_grog[PLAYER_1_WALK].timeScale = 1;
          animaciones_grog[PLAYER_1_WALK].paused = false;
          animaciones_grog[PLAYER_1_WALK].play();
     } else if (keys["S"]) {
          players[PLAYER_1].forward = speed;
          isMoving = true;
          animaciones_grog[PLAYER_1_WALK].timeScale = -1;
          animaciones_grog[PLAYER_1_WALK].paused = false;
          animaciones_grog[PLAYER_1_WALK].play();
     }

     if(keys["E"]){
          if(!isMoving && !isAttacking1){
               animaciones_grog[4].clampWhenFinished = true;
               animaciones_grog[4].play();     
               attackFlag = true; 
               swordslash.play();
               attackFlag = true;
          }
     }

     if(worldReady){
          if (animaciones_grog[4].isRunning()){
               isAttacking1 = true;
          }else{
               isAttacking1 = false;
               animaciones_grog[4].stop();

               if(playerInRange && attackFlag){
                    enemyHealth -= player1_damage; 
                    attackFlag = false;
                    console.log(enemyHealth);
               }

          }
     }

     if(speedparticle.isGeneratingParticles){
          speedparticle.animCounter += deltaTime;
          particleSpeedSystem.rotation.y += THREE.Math.degToRad(10 * deltaTime);
          
          if(speedparticle.animCounter >= speedparticle.particleDuration){
               speedparticle.animCounter = 0;
               speedparticle.isGeneratingParticles = false;
               particleSpeedSystem.parent.remove(particleSpeedSystem);
               speed = 5;
          }
     }

     if(healthparticle.isGeneratingParticles){
          healthparticle.animCounter += deltaTime;
          particleHealthSystem.rotation.y += THREE.Math.degToRad(10 * deltaTime);
          
          if(healthparticle.animCounter >= healthparticle.particleDuration){
               healthparticle.animCounter = 0;
               healthparticle.isGeneratingParticles = false;
               particleHealthSystem.parent.remove(particleHealthSystem);
          }
     }

     if(healingparticle.isGeneratingParticles){
          healingparticle.animCounter += deltaTime;
          particleHealingSystem.rotation.y += THREE.Math.degToRad(10 * deltaTime);
          
          if(healingparticle.animCounter >= healingparticle.particleDuration){
               healingparticle.animCounter = 0;
               healingparticle.isGeneratingParticles = false;
               players[PLAYER_1].remove(particleHealingSystem);
          }
     }
 
     if(iceparticle.isGeneratingParticles){
          iceparticle.animCounter += deltaTime;
          particleIceSystem.rotation.y += THREE.Math.degToRad(10 * deltaTime);
          
          if(iceparticle.animCounter >= iceparticle.particleDuration){
               iceparticle.animCounter = 0;
               iceparticle.isGeneratingParticles = false;
               enemies[0].followTo.remove(particleIceSystem);
          }
     }

     //player2
     if(coop){
          if (keys["J"]) {
               players[PLAYER_2].rotation.y += TURN_SPEED;
          } else if (keys["L"]) {
               players[PLAYER_2].rotation.y -= TURN_SPEED;
          }
          if (keys["I"]) {
               players[PLAYER_2].forward = -speed;
               animaciones_grog[PLAYER_2_WALK].timeScale = 1;
               animaciones_grog[PLAYER_2_WALK].paused = false;
               animaciones_grog[PLAYER_2_WALK].play();
               isMoving2 = true;
          } else if (keys["K"]) {
               players[PLAYER_2].forward = speed;
               animaciones_grog[PLAYER_2_WALK].timeScale = -1;
               animaciones_grog[PLAYER_2_WALK].paused = false;
               animaciones_grog[PLAYER_2_WALK].play();
               isMoving2 = true;
          }
     }

     if(keys["U"]){
          healing.play();

          if(!isMoving2 && !isAttacking2)
          animaciones_grog[PLAYER_2_HEAL].clampWhenFinished = true;
          animaciones_grog[PLAYER_2_HEAL].play();     
          healFlag = true;

          if(!healingparticle.isGeneratingParticles){
               healingparticle.isGeneratingParticles = true;
               particleHealingSystem = healingparticle.spawnParticles();
               
               players[PLAYER_1].add(particleHealingSystem);
          }

     }


          if(worldReady){
               
               if(coop){
                    if(isMoving || isMoving2)
                    footstepsAudio.play();
                    else
                    footstepsAudio.pause();
                    
                    if(!isMoving)
                    animaciones_grog[PLAYER_1_WALK].paused = true;
                    
                    if(!isMoving2)
                    animaciones_grog[PLAYER_2_WALK].paused = true;
                    
                    

               } else {
                    if(isMoving)
                    footstepsAudio.play();
                    else if(!isMoving){
                         animaciones_grog[1].paused = true;
                         footstepsAudio.pause();
                    }
               }
               
          }

     // Grog puede moverse solo si ya se ha cargado el mundo.
     // if(worldReady){
     // var grog = scene.getObjectByName("anchor");
     // grog.rotation.y += yaw * deltaTime;
     // grog.translateZ(forward * deltaTime);
     
     // }
     if(coop){
          if(worldReady){
               if (animaciones_grog[PLAYER_2_HEAL].isRunning()){
                    isAttacking2 = true;
               }else{
                    isAttacking2 = false;
                    animaciones_grog[PLAYER_2_HEAL].stop();
     
                    if(playerInRange && healFlag){
                         if(player1Health < 100){
                              player1Health += PLAYER2_HEALING; 
                              score -= 10;
                              $(".df_score").text(score);
                              $("#input_score").val(score);
                              $("#df_firstplayer-bar").css("width", `${player1Health}%`);
                         }

                         healFlag = false;
                         console.log(player1Health);
                    }
     
               }
          }
     }

          
     if(worldReady){
          var player1_ = scene.getObjectByName("anchor");
          player1_.rotation.y += player1_.yaw * deltaTime;
          player1_.translateZ(player1_.forward * deltaTime);
     
          if(coop){
               var player2_ = scene.getObjectByName("anchor2");
               player2_.rotation.y += player2_.yaw * deltaTime;
               player2_.translateZ(player2_.forward * deltaTime);
          }



          // -------------------------------  AI  -----------------------------
          if(!deadEnemy){
               //Si no ha visto ningun grog entrera aqui, es decir, seguira patrullando
               if(enemies[0].patrol){
                    if(!enemyAnimations[ENEMY_WALKING].isRunning())
                    enemyAnimations[ENEMY_WALKING].play();
                    //Seguira yendo de aqui para alla en el eje z
                    if(enemies[0].position.z <= -10 && enemyForward){
                         enemies[0].forward = ENEMY_SPEED;
                         enemies[0].rotation.y = THREE.Math.degToRad(0);
                    }
                    else{
                         enemyForward = false; 
                         enemies[0].forward = ENEMY_SPEED;
                         enemies[0].rotation.y = THREE.Math.degToRad(180);
                         
                         if(enemies[0].position.z <= -60){
                              enemyForward = true;
                         } 
                    }

                    //Lo estaremos moviendo
                    enemies[0].translateZ(enemies[0].forward * deltaTime)

                    //Si esta mirando hacia enfrente el rayo para detectar si ve algun grog se instancia normal
                    if(enemies[0].rotation.y == THREE.Math.degToRad(0)) rayCaster.set(enemies[0].position, enemies[0].vector);
                    //Si esta mirando de espaldas volteamos la z del vector 
                    else rayCaster.set(enemies[0].position, new THREE.Vector3(enemies[0].vector.x, enemies[0].vector.y, -enemies[0].vector.z));

                    let collide = rayCaster.intersectObjects(objetosColisionJugadores, true);

                    //Si vio algun grog entonces dejara de patrullar y el le setearemos una propiedad al enemigo con el objeto padre del grog, con el haremos que siempre nos este mirando el moco
                    if(collide.length > 0){
                         enemies[0].patrol = false;
                         enemies[0].followTo = collide[0].object.parent.parent;
                    }

               } else {
                    //Entonces aqui siempre nos estara mirando y si esta lo suficientemente lejos, se acercara hasta estar a 4 unidades de distancia.
                    enemies[0].lookAt(enemies[0].followTo.position);
                    if(enemies[0].position.distanceTo(enemies[0].followTo.position) > 5.0){
                         enemies[0].translateZ(ENEMY_SPEED * deltaTime)
                         playerInRange = false
                         enemyAnimations[ENEMY_ATTACK].stop();
                         enemyAnimations[ENEMY_ATTACK].reset();
                         
                         if(!enemyAnimations[ENEMY_WALKING].isRunning())
                         enemyAnimations[ENEMY_WALKING].play();
                    }
                    //En este else deberia atacar.
                    else{
                         enemyAnimations[ENEMY_WALKING].stop();
                         enemyAnimations[ENEMY_ATTACK].play();
                         if(!iceparticle.isGeneratingParticles){

                              iceparticle.isGeneratingParticles = true;
                              particleIceSystem = iceparticle.spawnParticles();
                              
                              //players[PLAYER_1].add(particleIceSystem);
                              enemies[0].followTo.add(particleIceSystem);
                              
                              player1Health -= ENEMY_DAMAGE; 

                              if(speed > 1)
                              speed -= 1;
                              helpgrog.play();
                              console.log(player1Health);

                              score -= 15

                              $(".df_score").text(score);
                              $("#input_score").val(score);
                              $("#df_firstplayer-bar").css("width", `${player1Health}%`);
                         }
                         playerInRange = true;
                    } 

                    if(enemyHealth <= 0){
                         enemyAnimations[ENEMY_ATTACK].stop();
                         enemyAnimations[ENEMY_IDLE].stop();     
                         enemyAnimations[ENEMY_DEAD].setLoop(THREE.LoopOnce);
                         enemyAnimations[ENEMY_DEAD].play();
                         console.log("dead")
                         score += 200;
                         $(".df_score").text(score);
                         $("#input_score").val(score);
                         deadEnemy = true;
                    }

               }    
          }else{
               if(!enemyAnimations[ENEMY_DEAD].isRunning()){
                    enemyAnimations[ENEMY_IDLE].play(); 
               }
                    if(enemyScalar > 0.005){
                         enemies[0].scale.setScalar(enemyScalar);
                         enemies[0].translateY(-0.005);
                         enemyScalar -= .0005
                         
                    }

                    if(enemyScalar <= 0.005){
                         enemies[0].translateY(-2);
                    }
               //}
          }

          // ------------------------------------------------------------------------------


          

          for(var i=0; i< cameras[PLAYER_1].vector.length; i++){
               var vector = cameras[PLAYER_1].vector[i];
               // 1er parámetro: Desde qué punto es lanzado el rayo o vector 
               // 2do parámetro: rayo o vector
               rayCaster.set(player1_.position, vector)

               // True nos sirve cuando también queremos saber si se colisionó con los hijos de estos objetos
               var collide = rayCaster.intersectObjects(objetosConColision, true);

               if(collide.length > 0 && collide[0].distance < 2.5){
                    //console.log("COLISIONASTE");
                    player1_.translateZ(-oldPosition[PLAYER_1].forward * deltaTime);
                    //collide[0].object.parent.collision = true;
               }
          }


               //COLISIONES PARA EL WIN
               for(var i=0; i< cameras[PLAYER_1].vector.length; i++){
                    var vector = cameras[PLAYER_1].vector[i];
                    // 1er parámetro: Desde qué punto es lanzado el rayo o vector 
                    // 2do parámetro: rayo o vector
                    rayCaster.set(player1_.position, vector)
     
                    // True nos sirve cuando también queremos saber si se colisionó con los hijos de estos objetos
                    var collide = rayCaster.intersectObject(scene.getObjectByName("win_zone"), true);
     
                    if(collide.length > 0 && collide[0].distance < 2.5){
                         if(!gameFinished){
                              score += 100;
                              $(".df_score").text(score);
                              $("#input_score").val(score);
                              $('#df_score-container').fadeIn(500);
                              clock.stop();
                         }

                         gameFinished = true;
                    }
               }
     
     
               //COLISIONES PARA LOS ITEMS
               for(var i=0; i< cameras[PLAYER_1].vector.length; i++){
                    var vector = cameras[PLAYER_1].vector[i];
                    // 1er parámetro: Desde qué punto es lanzado el rayo o vector 
                    // 2do parámetro: rayo o vector
                    rayCaster.set(player1_.position, vector)
     
                    // True nos sirve cuando también queremos saber si se colisionó con los hijos de estos objetos
                    var collide = rayCaster.intersectObjects(itemsCollision, true);
     
                    if(collide.length > 0 && collide[0].distance < 2.5){
     
                         if(collide[0].object.name == "strength_item" && !collide[0].object.grab){
     
                              if(!strengthparticle.isGeneratingParticles){
                                   strengthparticle.isGeneratingParticles = true;
                                   particleStrengthSystem = strengthparticle.spawnParticles();
                                   
                                   player1_damage = 20;
                                   score += 50;
                                   $(".df_score").text(score);
                                   $("#input_score").val(score);
                                   players[PLAYER_1].add(particleStrengthSystem);
                              }
                              
                         } else if(collide[0].object.name == "speed_item" && !collide[0].object.grab){
     
                              if(!speedparticle.isGeneratingParticles){
                                   speedparticle.isGeneratingParticles = true;
                                   particleSpeedSystem = speedparticle.spawnParticles();
     
                                   speed += 5;
                                   score += 50;
                                   $(".df_score").text(score);
                                   $("#input_score").val(score);
                                   players[PLAYER_1].add(particleSpeedSystem);
                              }
     
                         } else if(collide[0].object.name == "health_item" && !collide[0].object.grab && player1Health < 100){
     
                              if(!healthparticle.isGeneratingParticles){
                                   healthparticle.isGeneratingParticles = true;
                                   particleHealthSystem = healthparticle.spawnParticles();
                              
                                   player1Health += 20; 
                                   score += 50;
                                   
                                   if(player1Health > 100)
                                   player1Health = 100;
                                   
                                   $(".df_score").text(score);
                                   $("#input_score").val(score);
                                   $("#df_firstplayer-bar").css("width", `${player1Health}%`);
                                   
                                   players[PLAYER_1].add(particleHealthSystem);
                              }
                              
                         }
                         
                         collide[0].object.grab = true;
                         scene.remove(collide[0].object.parent);
                    }
               }



          // COLISIONES PARA EL JUGADOR 2
          if(coop){
               for(var i=0; i< cameras[PLAYER_2].vector.length; i++){
               var vector = cameras[PLAYER_2].vector[i];
               // 1er parámetro: Desde qué punto es lanzado el rayo o vector 
               // 2do parámetro: rayo o vector
               rayCaster.set(player2_.position, vector)

               // True nos sirve cuando también queremos saber si se colisionó con los hijos de estos objetos
               var collide = rayCaster.intersectObjects(objetosConColision, true);

                    if(collide.length > 0 && collide[0].distance < 2.5){
                         player2_.translateZ(-oldPosition[PLAYER_2].forward * deltaTime);
                         //collide[0].object.parent.collision = true;
                    }
               }


                //COLISIONES PARA EL WIN
                for(var i=0; i< cameras[PLAYER_2].vector.length; i++){
                    var vector = cameras[PLAYER_2].vector[i];
                    // 1er parámetro: Desde qué punto es lanzado el rayo o vector 
                    // 2do parámetro: rayo o vector
                    rayCaster.set(player2_.position, vector)

                    // True nos sirve cuando también queremos saber si se colisionó con los hijos de estos objetos
                    var collide = rayCaster.intersectObject(scene.getObjectByName("win_zone"), true);
                    if(collide.length > 0 && collide[0].distance < 2.5){

                         if(!gameFinished){
                              score += 100;
                              $(".df_score").text(score);
                              $("#input_score").val(score);
                              $('#df_score-container').fadeIn(500);
                              clock.stop();
                         }

                         gameFinished = true;
                    }
               }


               //COLISIONES PARA LOS ITEMS
               for(var i=0; i< cameras[PLAYER_2].vector.length; i++){
                    var vector = cameras[PLAYER_2].vector[i];
                    // 1er parámetro: Desde qué punto es lanzado el rayo o vector 
                    // 2do parámetro: rayo o vector
                    rayCaster.set(player2_.position, vector)
     
                    // True nos sirve cuando también queremos saber si se colisionó con los hijos de estos objetos
                    var collide = rayCaster.intersectObjects(itemsCollision, true);
     
                    if(collide.length > 0 && collide[0].distance < 2.5){
     
                         if(collide[0].object.name == "strength_item" && !collide[0].object.grab){
     
                              if(!strengthparticle.isGeneratingParticles){
                                   strengthparticle.isGeneratingParticles = true;
                                   particleStrengthSystem = strengthparticle.spawnParticles();
                                   
                                   player1_damage = 20;
                                   score += 50;
                                   players[PLAYER_1].add(particleStrengthSystem);
                                   $(".df_score").text(score);
                                   $("#input_score").val(score);
                              }
                              
                         } else if(collide[0].object.name == "speed_item" && !collide[0].object.grab){
     
                              if(!speedparticle.isGeneratingParticles){
                                   speedparticle.isGeneratingParticles = true;
                                   particleSpeedSystem = speedparticle.spawnParticles();

                                   speed = 10;
                                   score += 50;
                                   players[PLAYER_2].add(particleSpeedSystem);
                                   $(".df_score").text(score);
                                   $("#input_score").val(score);
                              }
     
                         } else if(collide[0].object.name == "health_item" && !collide[0].object.grab && player1Health < 100){
     
                              if(!healthparticle.isGeneratingParticles){
                                   healthparticle.isGeneratingParticles = true;
                                   particleHealthSystem = healthparticle.spawnParticles();

                                   player1Health += 20; 
                                   score += 50;
                                   if(player1Health > 100)
                                   player1Health = 100;
                                   
                                   $("#df_firstplayer-bar").css("width", `${player1Health}%`);
                                   $(".df_score").text(score);
                                   $("#input_score").val(score);

                                   players[PLAYER_2].add(particleHealthSystem);
                              }
                              
                         }
                         
                         collide[0].object.grab = true;
                         scene.remove(collide[0].object.parent);
                    }
               }



          }
          
          
     }

     renderers[0].render(scene, cameras[PLAYER_1]);
     if(coop)
     renderers[1].render(scene, cameras[PLAYER_2]);
     updateSkybox();
     
}

// Create SkyBox
function createSkyBox(){
     //SKYBOX
     let textura_ft = new THREE.TextureLoader().load('../imgs/skybox/heather_ft.jpg');
     let textura_bk = new THREE.TextureLoader().load('../imgs/skybox/heather_bk.jpg');
          let textura_up = new THREE.TextureLoader().load('../imgs/skybox/heather_up.jpg');
          let textura_dn = new THREE.TextureLoader().load('../imgs/skybox/heather_dn.jpg');
          let textura_rt = new THREE.TextureLoader().load('../imgs/skybox/heather_rt.jpg');
          let textura_lf = new THREE.TextureLoader().load('../imgs/skybox/heather_lf.jpg');
          
          materialArray.push(new THREE.MeshBasicMaterial({map: textura_ft}));
          materialArray.push(new THREE.MeshBasicMaterial({map: textura_bk}));
          materialArray.push(new THREE.MeshBasicMaterial({map: textura_up}));
          materialArray.push(new THREE.MeshBasicMaterial({map: textura_dn}));
          materialArray.push(new THREE.MeshBasicMaterial({map: textura_rt}));
          materialArray.push(new THREE.MeshBasicMaterial({map: textura_lf}));
          
          for(let i=0; i<6; i++){
               materialArray[i].side = THREE.BackSide;
          }
}

function createCamera(){
     camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 120);
     //camera.position.set(0, 0, 4);
     cameras.push(camera);
}

function createRender(){
     var screens;
     // Single Player, una pantalla.
     // CO-OP, dos pantallas. 
     coop ? screens = 2 : screens = 1;
     // Creo mi renderer
     renderer = new THREE.WebGLRenderer(); 
     // El tamaño de la pantalla depende de la cantidad de jugadores que va a haber.
     renderer.setSize(window.innerWidth/screens, window.innerHeight); 
     // Sin esto no jala, no pregunten por qué 
     document.body.appendChild(renderer.domElement);
     
     
     // Meto el renderer a mi arreglo de renderers:D 
     renderers.push(renderer);
}

// Update SkyBox
var updateSkybox = function(){
     skybox.rotation.y += 0.002;
};

// Cargar OBJ con textura
function loadOBJWithMTL(path, objFile, mtlFile, onLoadCallback) {
     var mtlLoader = new MTLLoader();
     mtlLoader.setPath(path);
     mtlLoader.load(mtlFile, (materials) => {
          
          var objLoader = new OBJLoader();
          objLoader.setMaterials(materials);
          objLoader.setPath(path);
          objLoader.load(objFile, (object) => {
               onLoadCallback(object);
          });
          
     });
}


class LoadModelDemo {
     constructor() {
          this._Initialize();
     }
     
     _Initialize() {
     
          this._mixers = [];
          this._previousRAF = null;
     
          this._LoadAnimatedModel3(); //blue monster
          //  this._LoadAnimatedModel4();//blue monster
          this._LoadAnimatedModel5();//GROG
          if(coop)
          this._LoadAnimatedModel6();//GROG2 SIIIIIIII SUFRIR MEMORIA
          this._RAF();
     }
     
     _LoadAnimatedModel3() {
          const loader = new FBXLoader();
          loader.setPath('../objs/monster_ice/');
          loader.load('monster3_Tpose.fbx', (fbx) => {
          fbx.scale.setScalar(0.05);
     
          const anim = new FBXLoader();
          anim.setPath('../objs/monster_ice/');
          anim.load('monster3_orc_idle.fbx', (anim) => {
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const idle = m.clipAction(anim.animations[0]);
               enemyAnimations[ENEMY_IDLE] = idle 
               idle.play();
          });

          const anim2 = new FBXLoader();
          anim2.setPath('../objs/monster_ice/');
          anim2.load('monster3_death.fbx', (anim2) => {
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const death = m.clipAction(anim2.animations[0]);
               enemyAnimations[ENEMY_DEAD] = death
          });

          const anim3 = new FBXLoader();
          anim3.setPath('../objs/monster_ice/');
          anim3.load('monster3_walk.fbx', (anim3) => {
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const walking = m.clipAction(anim3.animations[0]);
               enemyAnimations[ENEMY_WALKING] = walking
          });

          const anim4 = new FBXLoader();
          anim4.setPath('../objs/monster_ice/');
          anim4.load('monster3_attack.fbx', (anim4) => {
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const walking = m.clipAction(anim4.animations[0]);
               enemyAnimations[ENEMY_ATTACK] = walking
          });

          fbx.rotation.y = THREE.Math.degToRad(0);
          fbx.position.set(6, -0.5, -25)

          fbx.name = "monster";

          fbx.vector = new THREE.Vector3(0,0,1);
          fbx.patrol = true;

          //Creamos un material transparente para el cubo que estara cachando las intersecciones del raycaster
          const geometry = new THREE.BoxGeometry( 150, 150, 150 );
          const material = new THREE.MeshBasicMaterial({
               color: new THREE.Color(1.0,0.0,0.0),
          });

          material.transparent = true;
          material.opacity = 0.0;

          const cube = new THREE.Mesh( geometry, material );

          fbx.add( cube );
          enemies.push(fbx);

          scene.add(fbx);
     //     objetosConColision.push(fbx);
     
          });
     }
  
     _LoadAnimatedModel5() {
          const loader = new FBXLoader();
          loader.setPath('../objs/grog/');
          //Modelo y esqueleto
          loader.load('grog_Tpose_sword.fbx', (fbx) => {
          fbx.scale.setScalar(0.007);
     
          const anim = new FBXLoader();
          anim.setPath('../objs/grog/');
          //Animacion
          anim.load('grog_idle_sword.fbx', (anim)=>{
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const idle = m.clipAction(anim.animations[0]);
               idle.name = "idle";
               animaciones_grog[PLAYER_1_IDLE] = idle;
               idle.play();
          });

          const anim2 = new FBXLoader();
          anim2.setPath('../objs/grog/');
          anim2.load('grog_walk_sword.fbx', (anim2)=>{
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const walk = m.clipAction(anim2.animations[0]);
               animaciones_grog[PLAYER_1_WALK] = walk;
          });

          const animAttack = new FBXLoader();
          animAttack.setPath('../objs/grog/');
          animAttack.load('grog_attack_sword.fbx', (animAttack)=>{
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const attack = m.clipAction(animAttack.animations[0]);
               animaciones_grog[PLAYER_1_ATTACK] = attack;
               animaciones_grog[PLAYER_1_ATTACK].setLoop(THREE.LoopOnce);
          });
          
          fbx.name = "grog";
          fbx.rotation.y = THREE.Math.degToRad(180);
          //  scene.add(fbx);
          //  var grog2 = fbx.clone(); 
          //  scene.add(grog2); 
          var player1 = new THREE.Object3D();
          player1.position.set(30, -0.25, -10);
          player1.name = "anchor";
          // var player2 = new THREE.Object3D();
          // player2.position.set(45, 0, 55);
          // player2.name = "anchor2";
          //Creamos un material transparente para el cubo que estara cachando las intersecciones del raycaster
          const geometry = new THREE.BoxGeometry( 500, 500, 500 );
          const material = new THREE.MeshBasicMaterial({
               color: new THREE.Color(1.0,0.0,0.0)
          });

          material.transparent = true;
          material.opacity = 0.0;

          const cube = new THREE.Mesh( geometry, material );
          
          // El ancla es padre de Grog
          player1.add(fbx);
          // player2.add(grog2);
          fbx.add(cube);

          objetosColisionJugadores.push(cube);         
          // Modifico la posición de la cámara para ponerla detrás de Grog
          cameras[PLAYER_1].position.y = 2;
          cameras[PLAYER_1].position.z = -.8;
          // cameras[1].position.y = 1.8;
          // cameras[1].position.z = 0;
          // El ancla es padre de la cámara, por lo tanto la cámara es hermana de Grog 
          player1.add(cameras[PLAYER_1]);
          // player2.add(cameras[1]);
                    
          scene.add(player1);
          // scene.add(player2);
          players.push(player1);
          // players.push(player2);
          });
     }
     
     //grog(2)medico
     _LoadAnimatedModel6() {
          const loader = new FBXLoader();
          loader.setPath('../objs/grog_medico/buenas/');
          //Modelo y esqueleto
          loader.load('grog_Med_cetro_B.fbx', (fbx) => {
          fbx.scale.setScalar(0.007);
     
          const anim = new FBXLoader();
          anim.setPath('../objs/grog_medico/buenas/');

          //Animacion de caminar 
          anim.load('grog_Med_cetro_B_idle.fbx', (anim) => {
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const idle = m.clipAction(anim.animations[0]);
               animaciones_grog[PLAYER_2_IDLE] = idle;
               idle.play();
          });

          const anim2 = new FBXLoader();
          anim2.setPath('../objs/grog_medico/buenas/');
          anim2.load('grog_Med_cetro_B_walk.fbx', (anim2)=>{
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const walk = m.clipAction(anim2.animations[0]);
               animaciones_grog[PLAYER_2_WALK] = walk;
          });

          const anim3 = new FBXLoader();
          anim3.setPath('../objs/grog_medico/buenas/');
          anim3.load('grog_Med_cetro_B_cure.fbx', (anim3)=>{
               const m = new THREE.AnimationMixer(fbx);
               this._mixers.push(m);
               const heal = m.clipAction(anim3.animations[0]);
               animaciones_grog[PLAYER_2_HEAL] = heal;
               animaciones_grog[PLAYER_2_HEAL].setLoop(THREE.LoopOnce);
          });

          
          fbx.name = "grog2";
          fbx.rotation.y = THREE.Math.degToRad(180);
          //  scene.add(fbx);
          //  var grog2 = fbx.clone(); 
          //  scene.add(grog2); 
          
          var player2 = new THREE.Object3D();
          player2.position.set(30, -0.25, -10);
          player2.rotation.y = THREE.Math.degToRad(90);
          player2.name = "anchor2";
                    
          //Creamos un material transparente para el cubo que estara cachando las intersecciones del raycaster
          const geometry = new THREE.BoxGeometry( 500, 500, 500 );
          const material = new THREE.MeshBasicMaterial({
               color: new THREE.Color(1.0,0.0,0.0)
          });

          material.transparent = true;
          material.opacity = 0.0;

          const cube = new THREE.Mesh( geometry, material );
          
          // El ancla es padre de Grog
          player2.add(fbx);

          fbx.add(cube);

          objetosColisionJugadores.push(cube);
                    
          // Modifico la posición de la cámara para ponerla detrás de Grog
          if(coop){
          cameras[PLAYER_2].position.y = 2;
          cameras[PLAYER_2].position.z = 0;
          }
          // El ancla es padre de la cámara, por lo tanto la cámara es hermana de Grog 
          player2.add(cameras[PLAYER_2]);
                    
          scene.add(player2);
     
          players.push(player2);
          worldReady = true;
          });
     }
     
     
     _RAF() {
          requestAnimationFrame((t) => {
          if (this._previousRAF === null) {
               this._previousRAF = t;
          }
     
          this._RAF();
          renderer.render(scene, camera);
          this._Step(t - this._previousRAF);
          this._previousRAF = t;
          });
     }
     
     _Step(timeElapsed) {
          const timeElapsedS = timeElapsed * 0.001;
          if (this._mixers) {
          this._mixers.map(m => m.update(timeElapsedS));
          }
     
     }
}

     

     
     
