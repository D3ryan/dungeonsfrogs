$(document).ready(function () {
    $(document).on('keydown', function (e) {
        if(e.which == 80 || e.KeyCode == 80){ //P
            $('#df_pause-container').fadeIn(500);
            $('.df_life-container').fadeOut(500);
            $(".df_display-score").fadeOut(500);
        }
        // else if(e.which == 79 || e.KeyCode == 79){ //Esto obviamente solo es para verlo ahorita O
        //     $('#df_score-container').fadeToggle(500);
        // }
    });

    $("#df_return-game").click(function (e) { //Regresa al juego
        e.preventDefault();
        $('#df_pause-container').fadeOut(500);
        $('.df_life-container').fadeIn(500);
        $(".df_display-score").fadeIn(500)
    });

    $("#df_return-pause").click(function (e) { //Regresa al menu pausa
        e.preventDefault();
        $('#df_options-container').fadeOut(500, function() {
            $('#df_pause-container').fadeIn(500);
        }); 
    });


    $("#df_options").click(function (e) { //Menu Opciones
        e.preventDefault();
        $('#df_pause-container').fadeOut(500, function () {
            $('#df_options-container').fadeIn(500);
        });
        
    });

});