import LocalStorage from "../JS/localstorage.js"

$(document).ready(function () {
    
    $("#a_jugador1").click(function (e) { 
        e.preventDefault();
        let savedConfiguration = LocalStorage.read();

        if(savedConfiguration !== null) {
            savedConfiguration.Coop = false;
        } else {
            savedConfiguration = {};
            savedConfiguration.Coop = false;
        }

        LocalStorage.save(savedConfiguration);

        window.location = "../HTML/Niveles.html";
    });

    $("#a_jugador2").click(function (e) { 
        e.preventDefault();
        let savedConfiguration = LocalStorage.read();
        
        if(savedConfiguration !== null) {
            savedConfiguration.Coop = true;
        } else {
            savedConfiguration = {};
            savedConfiguration.Coop = true;
        }

        LocalStorage.save(savedConfiguration);
        
        window.location = "../HTML/Niveles.html";
    });

});